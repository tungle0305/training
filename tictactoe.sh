#!/bin/bash


# ECHO COMMAND
# echo Hello World!


# VARIABLES
# NAME="Brad"
# echo "My name is $NAME"

# USER INPUT
# read -p "Enter name: " NAME
# echo "My name is $NAME"

declare -A arr
n=3

createBoard(){
	for ((i=1; i <= 3; i++)) do 
		for ((j=1; j <= 3;j++)) do
			arr[$i,$j]="-"
		done
	done
}


check_win(){
	win=1

 	# check row	
	for ((i=1; i<= 3; i++)) do
		win=1
		for ((j=1; j<= 3; j++)) do
#			printf "iterate row - i: %d, j: %d\n" $i $j
			
			if [[ ${arr[$i,$j]} != $1 ]];then
#				printf "row: i: %d, j: %d\n" $i $j
				win=0
				break		
			fi
		done
		if [[ $win -eq 1 ]];then
#			printf "i: %d\n" $i
			return 1 
		fi
	done

 	# check col	
	for ((i=1; i<= 3; i++)) do
		win=1
		for ((j=1; j<= 3; j++)) do
			if [[ ${arr[$j,$i]} != $1 ]];then
#				printf "col: i: %d, j: %d\n" $j $i
				win=0
				break		
			fi
		done
		if [[ $win -eq 1 ]];then
#			printf "i: %d\n" $i
			return 1 
		fi
	done
	
	# check diagonal
	win=1
	for ((i=1; i<= $n; i++)) do
		if [[ ${arr[$i,$i]} != $1 ]];then
#			printf "diagonal: i: %d, j: %d\n" $i $j
			win=0
			break		
		fi
	done
	if [[ $win -eq 1 ]];then
#		printf "i: %d\n" $i
		return 1
	fi

	# check diagonal
	win=1
	for ((i=1; i<= $n; i++)) do
		if [[ ${arr[$i,$(($n + 1 - $i))]} != $1 ]];then
#			printf "diagonal: i: %d, j: %d\n" $i $j
			win=0
			break		
		fi
	done
	if [[ $win -eq 1 ]];then
#		printf "i: %d\n" $i
		return 1
	fi
	
	return 0	
}

isFilled(){
	for ((i=1; i<= $n; i++)) do
		for ((j=1; j<= $n; j++)) do
			if [[ ${arr[$i,$j]} != "-" ]];then
				return 0		
			fi
		done
	done

	return 1
}

displayBoard(){
	
	for ((i=1; i<= $n; i++)) do
		for ((j=1; j<= $n; j++)) do
			if [[ $j -ne 3 ]];then
				printf " %s |" ${arr[$i,$j]}
			else
				printf " %s" ${arr[$i,$j]}
			fi
		done
		printf "\n"
		if [[ $i -ne 3 ]];then	
			echo "-----------"
		fi
	done
}

fillRandom(){
	x=$(($RANDOM % $n + 1))
	y=$(($RANDOM % $n + 1))

	while [[ ${arr[$x,$y]} != "-" ]];do 
		x=$(($RANDOM % $n + 1))
		y=$(($RANDOM % $n + 1))
	done
	arr[$x,$y]="O"
}

doUserMove(){
	row=-1
	col=-1
	read -p "Enter row (1-3): " row
	read -p "Enter col (1-3): " col
	
	while [[ ${arr[$row,$col]} != "-" ]];do 
		read -p "Enter row (1-3): " row
		read -p "Enter col (1-3): " col
	done
	arr[$row,$col]="X"
}



createBoard 3

play(){
	win=0
	move=0

	while [[ $win -ne 1 ]];do	
		displayBoard
		doUserMove
		move=$move+1

		check_win "X"
		local ans=$?

		if [[ $ans -eq 1 ]];then
			printf "YOU WON!!!\n"
			win=1
			break
		fi
		
		if [[ $move -eq 9 ]];then
			echo "DRAW"
			break
		fi

		fillRandom
		move=$move+1

		check_win "O"
		local ans=$?
		if [[ $ans -eq 1 ]];then
			printf "YOU LOSE!!!\n"
			displayBoard
			win=1
			break
		fi

	done
}


play


